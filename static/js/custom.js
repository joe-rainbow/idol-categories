/**
 * Created by vinay on 7/20/2016.
 */
function getCategoryDetails(id){
    var url ='http://localhost:9000/action=CategoryGetHierDetails&Category='.concat(id).concat('&ResponseFormat=json');
    var ret_result = "";
    var response = $.ajax({
        type: 'POST',
        url: url,
        success: function (result) {
            processResponse(result, id);
        }
    });
}
function processIDs(i, val){
    var id = val.getAttribute("id");
    getCategoryDetails(id);
}
function processResponse(result, id){
    console.log("Getting category details for cat = " + id);
    console.log(result);
    var status = result['autnresponse']['response']['$'];

    if (status == 'SUCCESS'){
        var name = result['autnresponse']['responsedata']['autn:category']['autn:name']['$'];
        var parent_id = result['autnresponse']['responsedata']['autn:category']['autn:id']['$'];
        var contains_children = result['autnresponse']['responsedata']['autn:category']['autn:numchildren']['$'] > 0;
        var num_children = result['autnresponse']['responsedata']['autn:category']['autn:numchildren']['$'];
        var children = [];
        if (contains_children){
            if (num_children == 1){
                var child_categories = result['autnresponse']['responsedata']['autn:category']['autn:children']['autn:child'];
                var childid = child_categories['autn:childid']['$'];
                var childname = child_categories['autn:childname']['$'];
                var num_grandchildren = child_categories['autn:numchildren']["$"];
                var child = {
                    'id': childid,
                    'name': childname,
                    'num_children': num_grandchildren
                }
                if (num_grandchildren > 0){
                    
                }
                else{
                    children.push(child);
                }

            }
        }
        var category = {
            'name': name,
            'id': parent_id,
            'children': children
        }
    }
}