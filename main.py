import json

from flask import Flask
from flask import render_template
from idol.IDOLACIMethods import aci_call,get_json

app = Flask(__name__)


@app.route('/')
def main():
    category = "1"
    root = get_category_details(category)
    json_details = get_json(category)
    response_result = json_details["autnresponse"]["response"]["$"]
    category_name = json_details["autnresponse"]["responsedata"]["autn:category"]["autn:name"]["$"]
    category_number_of_children = json_details["autnresponse"]["responsedata"]["autn:category"]["autn:numchildren"]["$"]
    category_id = json_details["autnresponse"]["responsedata"]["autn:category"]["autn:id"]["$"]
    children = json_details["autnresponse"]["responsedata"]["autn:category"]["autn:children"]["autn:child"]
    cat = type('Category', (object,), {'name':category_name, 'number_of_children': category_number_of_children, "id": category_id , "children": children})
    return render_template('index.html', parent=root, response=response_result, category=cat)


@app.route('/getCategoryDetails/<id>')
def get_category_by_id(id):
    content = get_json(id)
    return json.dumps(content, ensure_ascii=False)


def get_category_details(category):
    details = aci_call(category)
    ns = {'ns0': 'http://schemas.autonomy.com/aci/'}
    count_children = details.find("./responsedata/ns0:category/ns0:numchildren", ns).text
    root_node_name = details.find("./responsedata/ns0:category/ns0:name", ns).text
    root_node_id = details.find("./responsedata/ns0:category/ns0:id", ns).text
    children_nodes = []

    if int(count_children) > 0:
        children = details.findall("./responsedata/ns0:category/ns0:children/ns0:child", ns)
        for child in children:
            child_details = get_category_details(child.find("ns0:childid", ns).text)
            child_dict_item = {"details": child_details}
            children_nodes.append(child_dict_item)

    root = {"name": root_node_name, "id": root_node_id, "num_children": count_children, "children": children_nodes}
    return root


if __name__ == "__main__":
    app.run(debug=True)
