class Category():
	def __init__(self, display_name, id):
		self._displayname = display_name,
		self._id = id
		self.children = []

	def set_has_children(self, value):
		self._has_children = value

	def get_has_children(self):
		return self._has_children

	def set_children(self, children):
		self.children = children

	def get_children(self):
		return self.children

	def add_child_category(self, category):
		self.children.append(category)
